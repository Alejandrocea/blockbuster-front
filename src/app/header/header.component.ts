import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../login/login.service';
import { Cliente } from '../clientes/cliente';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  clientes: Cliente[];
  token: any;
  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit(): void {
  }
}
