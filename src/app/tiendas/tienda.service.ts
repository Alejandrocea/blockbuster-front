import { Injectable } from '@angular/core';
import { Tienda } from './tienda';
import { of, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TiendaService {
  private urlEndpoint: string = "http://localhost:8090/tienda";
  constructor(private http: HttpClient) { }

  getTiendas(): Observable<Tienda[]> {
    return this.http.get<Tienda[]>(this.urlEndpoint);
  }
}
