import { Component, OnInit } from '@angular/core';
import { Tienda } from './tienda';
import { TiendaService } from './tienda.service';

@Component({
  selector: 'app-tiendas',
  templateUrl: './tiendas.component.html',
  styleUrls: ['./tiendas.component.css']
})
export class TiendasComponent implements OnInit {

  tiendas: Tienda[];
  constructor(private tiendaService: TiendaService) { }

  ngOnInit(): void {
    this.tiendaService.getTiendas().subscribe(
      tiendas => this.tiendas = tiendas
    );
  }

}
