import { Company } from 'src/app/companies/company'


export class Juego {
    titulo: string;
    fechaLanzamientoJuego: string;
    categoriasJuegos: string;
    pegiEdadJuego: number;
    company: Company[];
}
