import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { JuegosService } from './juegos.service';
import { Juego } from './juego';
import { AlertService } from '../alert/alert.service';
import { Company } from '../companies/company';
import { CompanyService } from '../companies/company.service';


@Component({
  selector: 'app-juegoform',
  templateUrl: './juegoform.component.html',
  styleUrls: ['./juegoform.component.css']
})
export class JuegoformComponent implements OnInit {
  public juego: Juego = new Juego()
  public nuevoJuego: boolean;
  companies: Company[];
  
    constructor(private juegoService: JuegosService, private router: Router, private activatedRoute: ActivatedRoute, private companyService: CompanyService) { }
  
    ngOnInit(): void {
      this.cargarJuego();
      this.companyService.getCompanies().subscribe(companies => this.companies = companies)
  
    }
  
    public changeNuevoJuego(): void{
      this.nuevoJuego = !this.nuevoJuego;
    }
  
    cargarJuego(): void{
      this.activatedRoute.params.subscribe(params => {
        let titulo = params['titulo']
        if(titulo){
          this.changeNuevoJuego();
          this.juegoService.getJuego(titulo).subscribe((juego) => this.juego = juego
          )
        }
      })
    }
  
    public create(): void{
      this.juegoService.create(this.juego).subscribe(
        juego => this.router.navigate(['/Juegos'])
      )
    }
  
    public updateJuego(): void {
      this.juegoService.updateJuego(this.juego).subscribe(
        juego => {
          this.router.navigate(['/Juegos']);
        }
      )
    }

    compararComp(o1 : Company, o2: Company){

      return o1 === null || o2=== null? false: o1.nombreCompany === o2.nombreCompany;
    } 
  }
  