import { Injectable } from '@angular/core';
import { Juego } from './juego';
import { of, Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertService } from '../alert/alert.service';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class JuegosService {
  private urlEndpointJuego: string = "http://localhost:8090/juego";

  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'})

  constructor(private http:HttpClient, private alertService: AlertService) { }

  getJuegos(): Observable<Juego[]> {
    return this.http.get<Juego[]>(this.urlEndpointJuego).pipe(
      catchError(error => {
        console.error('error de juegoos');
        this.alertService.error(`Error al consultar juegoos: '"${error.message}"`, {autoClose: false, keepAferRouteChangen: true})
          return throwError(error);
      })
    );
  }

  create(juego: Juego): Observable<Juego>{
    this.alertService.success(`Creado el Juego: "${juego.titulo}" correctamente`, {autoClose: true, keepAfterRouteChange: true});
        return this.http.post<Juego>(this.urlEndpointJuego, juego, {headers: this.httpHeaders}).pipe(
          catchError(error => {
            console.error('error de juegooos');
            if(error.status == 400){
              error.error.errorMessage.replace('[', '').replace(']', '').split(', ').reverse().forEach(errorMessage => {
                this.alertService.error(errorMessage);
              });
            } else {
            this.alertService.error(`Error al editar juegos: "${error.message}"`)
            }
              return throwError(error);
          })
        );
  }

  getJuego(titulo): Observable<Juego>{
    return this.http.get<Juego>(`${this.urlEndpointJuego}/${titulo}`)
  }

  updateJuego(juego: Juego): Observable<Juego>{
    this.alertService.success(`Editado el Juego: "${juego.titulo}" correctamente`, {autoClose: true, keepAfterRouteChange: true});
      return this.http.put<Juego>(`${this.urlEndpointJuego}/${juego.titulo}`, juego, {headers: this.httpHeaders}).pipe(
      catchError(error => {
        console.error('error de juegooos');
        if(error.status == 400){
          error.error.errorMessage.replace('[', '').replace(']', '').split(', ').reverse().forEach(errorMessage => {
            this.alertService.error(errorMessage);
          });
        } else {
        this.alertService.error(`Error al editar juegos: "${error.message}"`)
        }
          return throwError(error);
      })
    );
  }

  deleteJuego(titulo): Observable<Juego>{
    return this.http.delete<Juego>(`${this.urlEndpointJuego}/${titulo}`)
  }
}
