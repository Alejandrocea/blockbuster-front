import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JuegoformComponent } from './juegoform.component';

describe('JuegoformComponent', () => {
  let component: JuegoformComponent;
  let fixture: ComponentFixture<JuegoformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JuegoformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JuegoformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
