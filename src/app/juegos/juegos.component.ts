import { Component, OnInit } from '@angular/core';
import { Juego } from './juego';
import { JuegosService } from './juegos.service';
import { Company } from 'src/app/companies/company';
import { CompanyService } from 'src/app/companies/company.service';
import { AlertService } from '../alert/alert.service';

@Component({
  selector: 'app-juegos',
  templateUrl: './juegos.component.html',
  styleUrls: ['./juegos.component.css']
})
export class JuegosComponent implements OnInit {

  juegos: Juego[];
  showtitulo: boolean = true;

  //juegos: any[] = [{	titulo: "Battlefield", fechaLanzamiento: "05/03/2022", categoriaJuego: "SHOOTER",  pegi: 18}]

  constructor(private juegosService: JuegosService, private alertService: AlertService) { }

  ngOnInit(): void {
    this.refreshjuegos();
  }

  switchTitulo(): void{
    this.showtitulo = !this.showtitulo;
  }

  delete(titulo: string): void{
    this.juegosService.deleteJuego(titulo).subscribe(
      response => {this.alertService.success('Se ha elimindado', {autoClose: true})
      this.refreshjuegos();
    }
    )
  }

  refreshjuegos(): void {
    this.juegosService.getJuegos().subscribe(
      juegos => this.juegos = juegos
    );
}
}
