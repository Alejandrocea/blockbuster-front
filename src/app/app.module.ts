import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { JuegosComponent } from './juegos/juegos.component';
import { HttpClientModule } from '@angular/common/http';
import { ClientesComponent } from './clientes/clientes.component';
import { ClienteService } from './clientes/cliente.service';
import { RouterModule, Routes } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { FormComponent } from './clientes/form.component';
import { FormsModule } from '@angular/forms';
import { StocksComponent } from './stocks/stocks.component';
import { CompaniesComponent } from './companies/companies.component';
import { JuegosService } from './juegos/juegos.service';
import { CompanyService } from './companies/company.service';
import { StockService } from './stocks/stock.service';
import { TiendasComponent } from './tiendas/tiendas.component';
import { TiendaService } from './tiendas/tienda.service';
import { AlertComponent } from './alert/alert.component';
import { JuegoformComponent } from './juegos/juegoform.component';
import { LoginComponent } from './login/login.component';
import { PaginatePipe } from './pipe/paginate.pipe';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatPaginatorModule } from '@angular/material/paginator'; 


const routes: Routes = [
  {path: '', redirectTo: '/clientes', pathMatch: 'full'},
  {path: 'clientes', component: ClientesComponent},
  {path: 'Juegos', component: JuegosComponent},
  {path: 'clientes/form', component: FormComponent},
  {path: 'juegos/juegoform', component: JuegoformComponent},
  {path: 'companies', component: CompaniesComponent},
  {path: 'stocks', component: StocksComponent},
  {path: 'tiendas', component: TiendasComponent},
  {path: 'clientes/form/:documentacionCliente', component: FormComponent},
  {path: 'juegos/juegoform/:titulo', component: JuegoformComponent},
  {path: 'login', component: LoginComponent}

]

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    JuegosComponent,
    ClientesComponent,
    HeaderComponent,
    FormComponent,
    StocksComponent,
    CompaniesComponent,
    TiendasComponent,
    AlertComponent,
    JuegoformComponent,
    LoginComponent,
    PaginatePipe
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatPaginatorModule
  ],
  providers: [ClienteService, JuegosService, CompanyService, StockService, TiendaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
