import { Injectable } from '@angular/core';
import { Stock } from './stock';
import { of, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StockService {
  private urlEndpoint: string = "http://localhost:8090/stock";
  constructor(private http: HttpClient) { }

  getStocks(): Observable<Stock[]> {
    return this.http.get<Stock[]>(this.urlEndpoint);
  }
}
