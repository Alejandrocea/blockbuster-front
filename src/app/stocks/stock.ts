import { Cliente } from '../clientes/cliente';
import { Juego } from '../juegos/juego';
import { Tienda } from '../tiendas/tienda';

export class Stock {
    referenciaPedidoStock: string;
    estadoStock: string;
    tienda: Tienda;
    juegos: Juego;
    clientes: Cliente;
}
