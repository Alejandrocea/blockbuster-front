import { Rol } from 'src/app/Rol/rol'

export class Cliente {
    username: string;
    password: string;
    nombreCliente: string;
    fechaNacimientoCliente: string;
    correoCliente: string;
    documentacionCliente: string;
    roles: Rol[];
}
