import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';
import { AlertService } from '../alert/alert.service';
import { LoginService } from '../login/login.service';
import { Router } from '@angular/router';
import { PageEvent } from '@angular/material/paginator';


@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

  clientes: Cliente[];
  token = localStorage.getItem('token')
  
  constructor(private clienteService: ClienteService, private alertService: AlertService, private loginService: LoginService, private router: Router) { }

  ngOnInit(): void {
    this.refreshClientes();
  }

  page_size: number = 5;
  page_number: number = 1;
  pageSizeOptions =[5,10,50,100]

  handlePage(e: PageEvent){
    this.page_size = e.pageSize;
    this.page_number = e.pageIndex + 1;
  }

  delete(documentacionCliente: string): void{
    this.clienteService.deleteCliente(documentacionCliente).subscribe(
      response => {this.alertService.success('Se ha elimindado', {autoClose: true})
      this.refreshClientes();
    }
    )
  }

  refreshClientes(): void {
    this.clienteService.getClientes().subscribe(
      clientes => this.clientes = clientes
    )
  }

  logout(): void {
    this.loginService.deletetoken()
    this.router.navigate(['/login']);
    this.token = localStorage.getItem('token')
  }
}
