import { Component, OnInit } from '@angular/core';
import { Cliente} from './Cliente';
import { ClienteService } from './cliente.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from '../alert/alert.service';
import { Rol } from '../Rol/rol';
import { RoleService } from '../Rol/role.service';



@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  public cliente: Cliente = new Cliente()
  public nuevoCliente: boolean;
  aRoles: Rol[];

  constructor(private clienteService: ClienteService, private router: Router, private activatedRoute: ActivatedRoute, private alertService: AlertService, private roleService: RoleService) { }

  ngOnInit(): void {
    this.cargarCliente();
    this.roleService.getRoles().subscribe( aRoles => this.aRoles = aRoles);
  }
  
  public changeNuevoCliente(): void{
    this.nuevoCliente = !this.nuevoCliente;
  }

  compararRol(o1: Rol, o2: Rol){
    return o1 === null || o2 === null? false: o1.roles === o2.roles;
  }

  cargarCliente(): void{
    this.activatedRoute.params.subscribe(params => {
      let documentacionCliente = params['documentacionCliente']
      if(documentacionCliente){
        this.changeNuevoCliente();
        this.clienteService.getCliente(documentacionCliente).subscribe((cliente) => this.cliente = cliente
        )
      }
      /*if(documentacionCliente){
        this.title = 'Editar Cliente';
        this.clienteService
      }*/
    })
  }

  public create(): void{
    this.clienteService.create(this.cliente).subscribe(
      cliente => this.router.navigate(['/clientes'])
    )
  }

  public updateCliente(): void {
    this.clienteService.updateCliente(this.cliente).subscribe(
      cliente => {
        this.router.navigate(['/clientes']);
      }
    )
  }
}
