import { Injectable } from '@angular/core';
//import { CLIENTES } from './clientes.json';
import { Cliente } from './cliente';
import { of, Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { AlertService } from '../alert/alert.service';
import { Router } from '@angular/router';
import { LoginService } from '../login/login.service';


@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  private urlEndpoint: string = "http://localhost:8090/cliente";

  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'})
  token = localStorage.getItem('token');
  constructor(private http: HttpClient, private alertService: AlertService, private router: Router, private loginService: LoginService) { }

  getClientes(): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(this.urlEndpoint, {headers: this.loginService.getAuthHeaders()}).pipe(
      catchError(error => {
        console.error(`error de getClientes: "${error.message}"`);
        if(error.status == 401){
          this.router.navigate(['/login']);
        } else {
          this.alertService.error(`Error al consultar clientes: '"${error.message}"`, {autoClose: false, keepAferRouteChangen: true})
        }
          return throwError(error);
      })
    );

  }

  create(cliente: Cliente): Observable<Cliente>{
    this.alertService.success(`Creado el Cliente: "${cliente.nombreCliente}" correctamente`, {autoClose: true, keepAfterRouteChange: true});
        return this.http.post<Cliente>(this.urlEndpoint+"/", cliente, {headers: this.httpHeaders}).pipe(
          catchError(error => {
            console.error('error de clieeeentes');
            if(error.status == 400){
              error.error.errorMessage.replace('[', '').replace(']', '').split(', ').reverse().forEach(errorMessage => {
                this.alertService.error(errorMessage);
              });
            } else {
            this.alertService.error(`Error al editar clientes: "${error.message}"`)
            }
              return throwError(error);
          })
        );
  }

  getCliente(documentacionCliente): Observable<Cliente>{
    return this.http.get<Cliente>(`${this.urlEndpoint}/${documentacionCliente}`)
  }

  updateCliente(cliente: Cliente): Observable<Cliente>{
    this.alertService.success(`Editado el Cliente: "${cliente.nombreCliente}" correctamente`, {autoClose: true, keepAfterRouteChange: true});
      return this.http.put<Cliente>(`${this.urlEndpoint}/${cliente.documentacionCliente}`, cliente, {headers: this.httpHeaders}).pipe(
      catchError(error => {
        console.error('error de clientes');
        if(error.status == 400){
          error.error.errorMessage.replace('[', '').replace(']', '').split(', ').reverse().forEach(errorMessage => {
            this.alertService.error(errorMessage);
          });
        } else {
        this.alertService.error(`Error al editar clientes: "${error.message}"`)
        }
          return throwError(error);
      })
    );
  }

  deleteCliente(documentacionCliente): Observable<Cliente>{
    return this.http.delete<Cliente>(`${this.urlEndpoint}/${documentacionCliente}`)
  }

  
}
