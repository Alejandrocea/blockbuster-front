import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from './login.service';
import { AlertService } from '../alert/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  credentials: any;

  constructor(private router: Router, private loginService: LoginService, private alertService: AlertService) { }

  ngOnInit(): void {
    this.credentials = {username: '', password: ''};
  }

  login(): void {
    this.loginService.save(this.credentials)
    this.alertService.info(`Bienvenido "${this.credentials.username}" :)`, {autoClose: true, keepAfterRouteChange: true});
    this.router.navigate(['/clientes']);
  }

  logout(): void {
    this.loginService.deletetoken()
    this.router.navigate(['/clientes']);
  }
}
