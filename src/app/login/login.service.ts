import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { AlertService } from '../alert/alert.service';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private alertService: AlertService) { }

  save(credentials: any): void{
   localStorage.setItem('token', btoa(`${credentials.username}:${credentials.password}`))
  }

  deletetoken(): void{
    localStorage.removeItem('token')
  }

  getAuthHeaders(): HttpHeaders{
    let token = localStorage.getItem('token');
    if(!token) {
      return null;
    }
    return new HttpHeaders({'Authorization': 'Basic ' + token});
  }
}
