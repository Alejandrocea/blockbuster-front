import { Injectable } from '@angular/core';
import { Rol } from './rol';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertService } from '../alert/alert.service';
import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class RoleService {
  private urlEndpoint: string = "http://localhost:8090/roles";

  constructor(private http: HttpClient) { }
  getRoles(): Observable<Rol[]> {
    return this.http.get(this.urlEndpoint).pipe(
      map( response => response as Rol[]));
    }
}
