import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  titles: string[] = ['blockbuster-front1', 'blockbuster-front2'];
  persons: any[] = [{name: 'Agustín', lastname: 'Hernández Pérez'}, {name: 'Pepito', lastname: 'Pérez'},{name: 'Juan', lastname: 'Chu'},{name: 'Lolo', lastname: 'González'}]; 
}
