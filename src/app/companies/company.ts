import { Juego } from 'src/app/juegos/juego'


export class Company {
    cifCompany: string;
    nombreCompany: string;
    juegos: Juego[];
}
