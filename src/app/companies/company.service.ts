import { Injectable } from '@angular/core';
import { Company } from './company';
import { of, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  private urlEndpoint: string = "http://localhost:8090/company";
  constructor(private http: HttpClient) { }

  getCompanies(): Observable<Company[]> {
    return this.http.get<Company[]>(this.urlEndpoint);
  }
}
